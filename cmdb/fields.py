
from django.db import models
from django.core import exceptions

from IPy import IP

def convert_to_IPy(cidr_string):
    try:
        return IP(cidr_string)
    except ValueError as error:
        raise exceptions.ValidationError(
            str(error),
            code='invalid_cidr',
            params={'value': cidr_string}
        )


class Cidr(models.Field):

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 256
        super().__init__(*args, **kwargs)

    def from_db_value(self, value, expression, connection):
        if value is None:
            return value

        return convert_to_IPy(value)

    def to_python(self, value):
        if isinstance(value, IP):
            return value

        return convert_to_IPy(value)

    def get_db_prep_save(self, value, connection, prepared=False):
        if isinstance(value, IP):
            return value.strNormal()

        return value

    def get_db_prep_value(self, value, connection, prepared=False):
        if isinstance(value, IP):
            return value.strNormal()

        return value

    def db_type(self, connection):
        return 'varchar(64)'

    def get_internal_type(self):
        return 'CharField'